# ZD-280539

https://gitlab.zendesk.com/agent/tickets/280539

## Steps to reproduce

1. Create project with primary branch `A`
   1. Commit changes to `A`: ff184d141b5463f7dbd55dbacce4e293b710b53e 
1. Create branch `A1`
   1. Commit changes to `A1`: c6dc63388fb8cd02ed13220f2134bd4f91618ce6
1. Start MR for `A1` -> `A`: !1
1. Commit changes to `A`
1. Rename branch `A` to `B`, using the WebUI
1. Rebase MR `A1` -> `A1` on `B`



## What the dog doin'?

https://www.youtube.com/watch?v=I1uGKLsMiDo
